#language: pt

Funcionalidade: Home 

    @testHome
    Cenário: Verificar se usuário está Offline
    Dado que estou na tela de "Meu saldo:"
    Quando o status estiver "Offline"
    Então deve ser exibido o texto "Você não começou o dia ainda"