package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

import screens.BaseScreen;
import screens.ScreenHome;
import screens.ScreenLogin;
import screens.ScreenRecuperarSenha;

public class LoginSteps {
    // Cenário: Verificar botão Entrar desabilitado e habilitado
    @Dado("que estou na tela {string}")
    public void que_estou_na_tela_de_Login(String string) throws MalformedURLException {
        assertTrue(ScreenLogin.verificarLogo(string));
    }

    @Quando("o botão {string} estiver desabilitado")
    public void o_botao_estiver_desabilitado(String string) throws MalformedURLException {
        assertTrue(ScreenLogin.verificarEntrarDesabilitado(string));
    }

    @Quando("preencho o campo Código de Acesso com {string}")
    public void preencho_o_campo_com_código_válido(String string) throws MalformedURLException {
        ScreenLogin.preencherCodigoAcesso(string);
    }

    @Quando("preencho o campo Senha com {string}")
    public void preencho_o_campo_com_senha_válida(String string) throws MalformedURLException {
        ScreenLogin.preencherSenha(string);
    }

    @Então("o botão {string} deve ficar habilitado")
    public void o_botao_deve_ficar_habilitado(String string) throws MalformedURLException {
        assertTrue(ScreenLogin.verificarEntrarHabilitado(string));
    }


    //Cenário: Verificar Código de acesso ou senha inválidos
    @Então("devo receber o feedback de erro {string}")
    public void devo_receber_o_feedback_de_erro(String string) throws MalformedURLException {
        assertTrue(ScreenLogin.verificarFeedbackErro(string));
    }


    //Cenário: Verificar botão Esqueci a senha
    @Quando("clico no botão {string}")
    public void clico_no_botão(String string) throws MalformedURLException {
        switch(string){
            case "Entrar":
                ScreenLogin.clicarEntrar(string);
                break;
            case "Esqueci a senha":
                ScreenLogin.esqueciSenha(string);
                break;
        }
    }

    @Então("devo ser redirecionado para a tela {string}")
    public void devo_ser_redirecionado_para_a_tela(String string) throws MalformedURLException {
        switch(string){
            case "Recuperar senha":
                assertTrue(ScreenRecuperarSenha.verificarRecuperarSenha(string));
                ScreenRecuperarSenha.clicarVoltar("Voltar");
                break;
            case "Home":
                assertTrue(ScreenHome.verificarHome(string));
                break;
        }
    }


    //Cenário: Verificar endpoint de Login com sucesso
    @Quando("chamar o endpoint {string}")
    public void chamar_o_endpoint(String string) throws MalformedURLException, IOException {
        
    }

    @Então("devo receber o status {string}")
    public void devo_receber_o_status(String string) throws MalformedURLException, IOException {
        assertEquals(BaseScreen.verificarApi(), 200);
    }
}