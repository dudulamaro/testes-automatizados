package steps;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.MalformedURLException;

import io.cucumber.java.it.Quando;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import screens.ScreenHome;
import screens.ScreenLogin;


public class HomeSteps {
    // Cenário: Verificar se usuário está Offline
    @Dado("que estou na tela de {string}")
    public void que_estou_na_tela_Home(String string) throws MalformedURLException {
        ScreenLogin.preencherCodigoAcesso("0161");
        ScreenLogin.preencherSenha("1234");
        ScreenLogin.clicarEntrar("Entrar");
        assertTrue(ScreenHome.verificarHome(string));
    }

    @Quando("o status estiver {string}")
    public void o_status_estiver_offline(String string) throws MalformedURLException {
        assertTrue(ScreenHome.verificarStatusOffline(string));
    }

    @Então("deve ser exibido o texto {string}")
    public void deve_ser_exibido_o_texto(String string) throws MalformedURLException {
        assertTrue(ScreenHome.verificarTextoNaoComecouDia(string));
    }
}