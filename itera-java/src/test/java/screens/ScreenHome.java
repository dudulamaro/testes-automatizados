package screens;

import java.net.MalformedURLException;
import org.openqa.selenium.WebElement;

public class ScreenHome {
    public static boolean verificarHome(String iteraHome) throws MalformedURLException {
        WebElement home = BaseScreen.localizarElemento(iteraHome);
        if (home.isDisplayed() == true) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean verificarStatusOffline(String status) throws MalformedURLException {
        WebElement status_home = BaseScreen.localizarElemento(status);
        if (status_home.isDisplayed() == true) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean verificarTextoNaoComecouDia(String texto) throws MalformedURLException {
        WebElement texto_home = BaseScreen.localizarElemento(texto);
        if (texto_home.isDisplayed() == true) {
            return true;
        }
        else {
            return false;
        }
    }
}
