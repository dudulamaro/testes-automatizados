package screens;

import java.net.MalformedURLException;
import org.openqa.selenium.WebElement;

public class ScreenLogin {
    public static boolean verificarLogo(String iteraLogo) throws MalformedURLException {
        WebElement logo = BaseScreen.localizarElemento(iteraLogo);
        if (logo.isDisplayed() == true) {
            return true;
        }
        else {
            return false;
        }
    }

    public static void preencherCodigoAcesso(String codigo) throws MalformedURLException {
        WebElement codigoAcesso = BaseScreen.localizarCampoTexto("Código de acesso");
        codigoAcesso.click();
        codigoAcesso.clear();
        codigoAcesso.sendKeys(codigo);
    }

    public static void preencherSenha(String senha) throws MalformedURLException {
        WebElement campoSenha = BaseScreen.localizarCampoTexto("Senha");
        campoSenha.click();
        campoSenha.clear();
        campoSenha.sendKeys(senha);
    }

    public static void exibirSenha(String exibirSenha) throws MalformedURLException {
        WebElement botaoExibirSenha = BaseScreen.localizarIcone(exibirSenha);
        botaoExibirSenha.click();
    }

    public static void esqueciSenha(String esqueciSenha) throws MalformedURLException {
        WebElement botaoEsqueciSenha = BaseScreen.localizarElemento(esqueciSenha);
        botaoEsqueciSenha.click();
    }

    public static void clicarEntrar(String entrar) throws MalformedURLException {
        WebElement botaoEntrar = BaseScreen.localizarElemento(entrar);
        botaoEntrar.click();
    }

    public static boolean verificarEntrarDesabilitado(String entrar) throws MalformedURLException {
        WebElement botaoEntrar = BaseScreen.localizarElemento(entrar);
        if (botaoEntrar.isSelected() == false) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean verificarEntrarHabilitado(String entrar) throws MalformedURLException {
        WebElement botaoEntrar = BaseScreen.localizarElemento(entrar);
        if (botaoEntrar.isEnabled() == true) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean verificarFeedbackErro(String erro) throws MalformedURLException {
        WebElement textoFeedbackErro = BaseScreen.localizarElemento(erro);
        if (textoFeedbackErro.isDisplayed() == true) {
            return true;
        }
        else {
            return false;
        }
    }
}