package screens;

import java.net.MalformedURLException;

import org.openqa.selenium.WebElement;

public class ScreenRecuperarSenha {
    public static boolean verificarRecuperarSenha(String recuperarSenha) throws MalformedURLException {
        WebElement textoRecuperarSenha = BaseScreen.localizarElemento(recuperarSenha);
        if (textoRecuperarSenha.isDisplayed() == true) {
            return true;
        }
        else {
            return false;
        }
    }

    public static void clicarVoltar(String voltar) throws MalformedURLException {
        WebElement botaoVoltar = BaseScreen.localizarIcone(voltar);
        botaoVoltar.click();
    }
}
