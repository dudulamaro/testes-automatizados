package screens;

import io.appium.java_client.AppiumBy;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;
import java.net.MalformedURLException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

import configs.ConfigTest;

public class BaseScreen {
    
    public static WebElement localizarElemento(String accessibilityId) throws MalformedURLException {
        WebDriverWait wait = new WebDriverWait(ConfigTest.getCurrentDriver(), Duration.ofSeconds(10));
        WebElement elemento = wait.until(ExpectedConditions.presenceOfElementLocated(AppiumBy.accessibilityId(accessibilityId)));
        return elemento;
    }

    public static WebElement localizarCampoTexto(String accessibilityId) throws MalformedURLException {
        WebElement campo = localizarElemento(accessibilityId);
        return campo.findElement(AppiumBy.className("android.widget.EditText"));
    }

    public static WebElement localizarIcone(String accessibilityId) throws MalformedURLException {
        WebElement icone = localizarElemento(accessibilityId);
        return icone.findElement(AppiumBy.className("android.widget.ImageView"));
    }

    public static int verificarApi() throws MalformedURLException, IOException {
        OkHttpClient client = new OkHttpClient();

        String body = "{\n" +
                    " \"acessCode\": \"0187\",\n" +
                    " \"password\": \"2104\"\n" +
                    "}";
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody requestBody = RequestBody.create(body, mediaType);

        Request request = new Request.Builder()
            .url("http://tools-memorian-api.usemobile.com.br")
            .post(requestBody)
            .build();

        Response response = client.newCall(request).execute();

        //System.out.println(response.body().string());
        //System.out.println(response.code());
        int code = response.code();

        return code;
        //System.out.println(response.headers());
    }
}