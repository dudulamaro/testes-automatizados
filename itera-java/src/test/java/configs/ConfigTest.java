package configs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.options.XCUITestOptions;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import io.appium.java_client.screenrecording.CanRecordScreen;
import org.apache.commons.codec.binary.Base64;

public class ConfigTest {
    static AppiumDriverLocalService service;
    public static AppiumDriver driver;
    public static String platform = "android";
    public static String appPackage = "br.com.usemobile.itera.dev";
    public static String appActivity = "br.com.usemobile.itera.MainActivity";
    public static String ipAdress = "127.0.0.1";
    public static int port = 4723;
    public static String basePath = "/wd/hub";

    public static void startServer() {
        service = new AppiumServiceBuilder()
            .withIPAddress(ipAdress)  
            .usingPort(port)
            .withArgument(GeneralServerFlag.BASEPATH, basePath)
            .build();
        service.start();
    }

    public static void stopServer() {
        service.stop();
    }

    public static AppiumDriver android_driver() throws MalformedURLException {
        UiAutomator2Options options = new UiAutomator2Options();
        options.setUdid("emulator-5554")
            .setAppPackage(appPackage)
            .setAppActivity(appActivity)
            .setPlatformName("Android")
            .setPlatformVersion("12.0")
            .setAutomationName("UiAutomator2")
            .setCapability("unicodeKeyboard", true);
        driver = new AndroidDriver(new URL("http://" + ipAdress + ":" + port + basePath), options);
        return driver;
    }

    public static AppiumDriver ios_driver() throws MalformedURLException {
        XCUITestOptions options = new XCUITestOptions()
            .setUdid("9ee27959d953a46e5b7d7da8e64f68b46b86db77")
            .setBundleId(appPackage)
            .setPlatformName("iOS")
            .setPlatformVersion("16.7")
            .setAutomationName("XCUITest");
        driver = new IOSDriver(new URL("http://" + ipAdress + ":" + port + basePath), options);
        return driver;
    }

    public static AppiumDriver getCurrentDriver() {
        return driver;
    }

    public static void quitCurrentDriver() {
        getCurrentDriver().quit();
    }

    public static void launchApp() throws MalformedURLException {
        if(platform.contains("ios")){
            ios_driver();
        }else if(platform.contains("android")){
            android_driver();
        }else
            System.out.println("Plataforma inválida, insira android ou ios!");
    }

    public static void startRecording() {
        ((CanRecordScreen) getCurrentDriver()).startRecordingScreen();
    }

    public static void stopRecording() throws IOException {
        String media = ((CanRecordScreen) getCurrentDriver()).stopRecordingScreen();
        String dirPath = Paths.get("target/reports").toString();
        File videoDir = new File(dirPath);
        FileOutputStream stream = null;       
        
        try {
            if (!videoDir.exists()) {
                videoDir.mkdirs();
            }
            if (!videoDir.canWrite()) {
                throw new IOException("O diretório de relatórios não pode ser escrito.");
            }

            stream = new FileOutputStream(new File(videoDir, "relatorio" + ".mp4"));

            stream.write(Base64.decodeBase64(media));

        } finally {
            if(stream != null) {
                stream.close();
            }
        }
    }
}
