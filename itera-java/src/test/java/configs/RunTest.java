package configs;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = "src/test/java/features",
    glue = "steps",
    monochrome = true,
    tags = "@testLogin",
    plugin = {"pretty", "html:target/reports/relatorio.html"}
)

public class RunTest {
    @BeforeClass
    public static void startProject() throws MalformedURLException {
        ConfigTest.startServer();
        ConfigTest.launchApp();
        ConfigTest.startRecording();
    }

    @AfterClass
    public static void finishProject() throws IOException {
        ConfigTest.stopRecording();
        ConfigTest.quitCurrentDriver();
        ConfigTest.stopServer();
    }
}
