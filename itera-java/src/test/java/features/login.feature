#language: pt

@testLogin
Funcionalidade: Login

    @all
    Cenário: Verificar botão Entrar desabilitado e habilitado
        Dado que estou na tela "Itera logo"
        Quando o botão "Entrar" estiver desabilitado
        Quando preencho o campo Código de Acesso com "0164"
        Quando preencho o campo Senha com "1234"
        Então o botão "Entrar" deve ficar habilitado

    @all
    Cenário: Verificar Código de acesso ou senha inválidos 
        Dado que estou na tela "Itera logo"
        Quando preencho o campo Código de Acesso com "0000"
        Quando preencho o campo Senha com "1111"
        Quando clico no botão "Entrar"
        Então devo receber o feedback de erro "Código ou senha inválidos"

    @all
    Cenário: Verificar botão Esqueci a senha
        Dado que estou na tela "Itera logo"
        Quando clico no botão "Esqueci a senha"
        Então devo ser redirecionado para a tela "Recuperar senha"

    @all
    Cenário: Verificar endpoint de Login com sucesso
        Dado que estou na tela "Itera logo"
        Quando chamar o endpoint "/login"
        Então devo receber o status "200"

    @all
    Cenário: Verificar Login com sucesso
        Dado que estou na tela "Itera logo"
        Quando preencho o campo Código de Acesso com "0164"
        Quando preencho o campo Senha com "1234"
        Quando clico no botão "Entrar"
        Então devo ser redirecionado para a tela "Home"